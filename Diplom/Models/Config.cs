﻿using System.IO;
using System.Text.Json;

namespace Diplom.Models
{
    class Config
    {
        private string PATHTOCONFIGFILE = "../../config.json";
        public ConfigModels ConfigModels { get; set; }

        public Config()
        {
            ConfigModels = JsonSerializer.Deserialize<ConfigModels>(File.ReadAllText(PATHTOCONFIGFILE));
            CreateDirectoryForDocuments();
            if (ConfigModels == null)
            {
                ConfigModels = new ConfigModels()
                {
                    PathToTemplatesDirectory = "../../документи/шаблони",
                    PathToFilesDirectory = "../../документи",
                };
                UpdateConfig();
            }
        }

        public void UpdateConfig(ConfigModels configModels = null)
        {
            if (configModels == null)
            {
                configModels = this.ConfigModels;
            }

            var str = JsonSerializer.Serialize<ConfigModels>(this.ConfigModels);
            File.WriteAllText(PATHTOCONFIGFILE, str);
        }

        private  void CreateDirectoryForDocuments()
        {
            if (!Directory.Exists("../../документи"))
            {
                Directory.CreateDirectory("../../документи");
            }
            if (!Directory.Exists("../../документи/шаблони"))
            {
                Directory.CreateDirectory("../../документи/шаблони");
            }
        }
    }
}
