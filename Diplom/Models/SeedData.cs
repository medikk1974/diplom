﻿using Diplom.DbContexts;
using Diplom.Entity;
using Diplom.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Models
{
    class SeedData
    {
        public static void SeedSubunit(DiplomDbContext db)
        {
            var subdevisions = db.Subunit.ToList();

            if (subdevisions.Count <= 1)
            {
                var mainSubdevision = new Subunit()
                {
                    Name = "Навчальні Заклади",
                    IsRoot = true,
                    Children = new ObservableCollection<Subunit>()
                    {
                        new Subunit
                        {
                            Name = "Військовий Інстетут (ВІТІ)",
                            Faculties = new ObservableCollection<Faculty>()
                            {

                                new Faculty()
                                {
                                    Name = "1 Факультет телекомунікаційних систем",
                                    Specialties = new ObservableCollection<Specialty>()
                                    {
                                        new Specialty()
                                        {
                                            EducationalProgram = "Електроніка та телекомунікації",
                                            EducationalProgramEn = "Electronics and telecommunications",

                                            Specialization = "Телекомунікації та радіотехніка",
                                            SpecializationEn = "Telecommunications and radio engineering",

                                            ProfessionalQualification = "телекомунікацій та радіо техніки",
                                            ProfessionalQualificationEn = "telecommunications and radio equipment",
                                          
                                            TrainingDirection = "",
                                            TrainingDirectionEn = "",
                                         
                                            Сode = "172"
                                        },
                                    }
                                },
                                new Faculty()
                                {
                                    Name = "2 Факультет інформаційних технологій",

                                     Specialties = new ObservableCollection<Specialty>()
                                     {
                                        new Specialty()
                                        {
                                            EducationalProgram = "Інформаційні технології",
                                            EducationalProgramEn = "Information Technology",

                                            Specialization = "Комп’ютерні науки",
                                            SpecializationEn = "Computer Science",

                                            ProfessionalQualification = "комп'ютерних наук",
                                            ProfessionalQualificationEn = "computer science",

                                            TrainingDirection = "",
                                            TrainingDirectionEn = "",

                                            Сode = "122"
                                        },

                                        new Specialty()
                                        {
                                            EducationalProgram = "Інформаційні технології",
                                            EducationalProgramEn = "Information Technology",

                                            Specialization = "Інформаційні системи та технології",
                                            SpecializationEn = "Information systems and technologies",

                                            ProfessionalQualification = "інформаційних системи та технологій",
                                            ProfessionalQualificationEn = "information Systems and Technology",
                                            TrainingDirection = "",
                                            TrainingDirectionEn = "",
                                            Сode = "126"
                                        }
                                     }
                                },
                                new Faculty()
                                {
                                    Name = "3 Факультет бойового застосування систем управління та зв'язку",
                                    Specialties = new ObservableCollection<Specialty>()
                                    {
                                        new Specialty()
                                        {
                                            EducationalProgram = "Воєнні науки, національна безпека, безпека державного кордону",
                                            EducationalProgramEn = "Military sciences, national security, state border security",

                                            Specialization = "Озброєння та військова техніка",
                                            SpecializationEn = "Weapons and military equipment",

                                            ProfessionalQualification = "озброєння та військової техніки",
                                            ProfessionalQualificationEn = "weapons and military equipment",

                                            TrainingDirection = "",
                                            TrainingDirectionEn = "",
                                            Сode = "255"
                                        },

                                        new Specialty()
                                        {
                                            EducationalProgram = "Воєнні науки, національна безпека, безпека державного кордону",
                                            EducationalProgramEn = "Military sciences, national security, state border security",

                                            Specialization = "Військове управління",
                                            SpecializationEn = "Military administration",

                                            ProfessionalQualification = "військового управління",
                                            ProfessionalQualificationEn = "Military administration",

                                            TrainingDirection = "",
                                            TrainingDirectionEn = "",

                                            Сode = "253"
                                        },

                                        new Specialty()
                                        {
                                            EducationalProgram = "Інформаційні технології",
                                            EducationalProgramEn = "Information Technology",

                                            Specialization = "Кібербезпека",
                                            SpecializationEn = "Cybersecurity",

                                            ProfessionalQualification = "кібербезпеки",
                                            ProfessionalQualificationEn = "сybersecurity",

                                            TrainingDirection = "",
                                            TrainingDirectionEn = "",

                                            Сode = "125"
                                        }
                                    }
                                },

                            },
                            HistoricalNames = new ObservableCollection<HistoricalName>()
                            {
                                new HistoricalName()
                                {
                                    DateChange = new DateTime(2019,1,1),
                                    Name = "Військовий інститут телекомунікацій та інформатизації імені Героїв Крут",
                                    NameEN = "Military Institute of Telecommunications and Information Technologies named after the Heroes of Kruty"
                                },
                             
                                new HistoricalName()
                                {
                                    DateChange = new DateTime(2010,1,1),
                                    Name = "Військовий інститут телекомінкацій та інформатизації",
                                    NameEN = "Militart Institut Telecomunication And Informatization"
                                }
                            },
                            DocumentType = new ObservableCollection<DocumentType>()
                            {
                                new DocumentType()
                                {
                                    Cvalification = "ступінь вищої освіти бакалавр",
                                    CvalificationEn = "Bachelor Degree",
                                    ProfCvalification = "бакалавр",
                                    ProfCvalificationEn = "Bachelor",
                                },

                                new DocumentType()
                                {
                                    Cvalification = "ступінь вищої освіти магістр",
                                    CvalificationEn = "Magister Degree",
                                    ProfCvalification = "магістер",
                                    ProfCvalificationEn = "Magister",
                                }
                            }

                        },

                        new Subunit
                        {
                            Name = "Коледж сержантського складу"
                        }
                    }
                };

                db.Subunit.Add(mainSubdevision);
                db.SaveChanges();
            }

            if (!db.Requisitions.Any())
            {
                db.Requisitions.Add(new Requisition()
                {
                    DateTimeCreate = new DateTime(2021, 02, 10),
                    FormEducation = "Дена",
                    DateEndEducation = new DateTime(2021, 02, 10),
                    SpecialtyId = 2,
                    FacultyId = 2,
                    DocumentTypeId = 1,
                    SubunitId = 2,
                    People = new ObservableCollection<Person>()
                    {
                        new Person()
                        {
                            Name = "Артем",
                            NameEn ="Artem",
                            Surname = "Жирук",
                            SurnameEn = "Zhyruk",
                            FatherName = "Васильович",

                            Sex = Sex.Man,

                            DateOfBirth = DateTime.Now,

                            DiplomaNumber = "0001",
                            DiplomaSeries = "LD001",

                            DocomentName = "2016 Паспорт громадянина України",
                            DocumentNumber = "00001",
                            DocumentSeries = "000023",

                        },

                        new Person()
                        {
                            Name = "Андрій",
                            NameEn ="Andrii",
                            Surname = "Журавель",
                            SurnameEn = "Zhyruk",
                            FatherName = "Васильович",

                            Sex = Sex.Man,

                            DateOfBirth = DateTime.Now,

                            DiplomaNumber = "0001",
                            DiplomaSeries = "LD001",

                            DocomentName = "2016 Паспорт громадянина України",
                            DocumentNumber = "00001",
                            DocumentSeries = "000023",

                        },


                        new Person()
                        {
                            Name = "Миколай",
                            NameEn ="Mykolai",
                            Surname = "Шевчук",
                            SurnameEn = "Shevchuk",
                            FatherName = "Михайлович",

                            Sex = Sex.Man,

                            DateOfBirth = DateTime.Now,

                            DiplomaNumber = "0002",
                            DiplomaSeries = "LD001",

                            DocomentName = "2016 Паспорт громадянина України",
                            DocumentNumber = "00001",
                            DocumentSeries = "000023",

                        },
                    }
                }) ;

                db.SaveChanges();
            }
        }
    }
}
