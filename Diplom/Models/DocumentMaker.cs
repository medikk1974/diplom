﻿using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TemplateEngine.Docx;

namespace Diplom.Models
{
    class DocumentMaker
    {
        public string TemplatePath { get; set; }
        public string DirectoryResaultPath { get; set; }

        public DocumentMaker(string templatePath, string directoryResaultPath)
        {
            TemplatePath = templatePath;
            DirectoryResaultPath = Directory.CreateDirectory(directoryResaultPath).FullName; 
        }

        public void CreateTemplate(Requisition requisition)
        {
            var routNewFile = CopyTemplate(requisition);

            using (var outputDocument = new TemplateProcessor(routNewFile).SetRemoveContentControls(true))
            {
                outputDocument.FillContent(ValueFornNewFile(requisition));
                outputDocument.SaveChanges();
            }
        }

        private string CopyTemplate(Requisition requisition)
        {
            var routTemplat = DirectoryResaultPath +"\\" + requisition.DateTimeCreate.ToString("mm/HH") + " " + requisition.Id + " " + requisition.Faculty.Name  +  ".docx";
            try
            {
                File.Copy(TemplatePath, routTemplat, true);
            }
            catch
            {
                MessageBox.Show("");
            }
            return routTemplat;
        }

        private Content ValueFornNewFile(Requisition requisition)
        {
            var tc = new TableContent("table");

            foreach (var item in requisition.People)
            {
                tc.AddRow(
                new FieldContent("DiplomaSeries", item.DiplomaSeries),
                new FieldContent("DiplomaNumber", item.DiplomaNumber),
                new FieldContent("Surname", item.Surname),
                new FieldContent("Name", item.Name),
                new FieldContent("FatherName", item.FatherName),
                new FieldContent("Sex", (item.Sex == Sex.Man) ? "в" : "ла"),
                new FieldContent("DateEndEducation", requisition.DateEndEducation.Year.ToString()),
                new FieldContent("HistoricalName", requisition.HistoricalName.Name),
                new FieldContent("Cvalification", requisition.DocumentType.Cvalification),
                new FieldContent("Speciality", requisition.Specialty.Specialization),
                new FieldContent("ProfCvalification", requisition.DocumentType.ProfCvalification),
                new FieldContent("ProfCvalificationOf", requisition.Specialty.ProfessionalQualification),

                new FieldContent("SurnameEn", item.SurnameEn),
                new FieldContent("NameEn", item.NameEn),
                new FieldContent("DateEndEducation", requisition.DateEndEducation.Year.ToString()),
                new FieldContent("HistoricalNameEn", requisition.HistoricalName.NameEN),
                new FieldContent("CvalificationEn", requisition.DocumentType.CvalificationEn),
                new FieldContent("SpecialityEn", requisition.Specialty.SpecializationEn),
                new FieldContent("ProfCvalificationEn", requisition.DocumentType.ProfCvalificationEn),
                new FieldContent("ProfCvalificationOfEn", requisition.Specialty.ProfessionalQualificationEn)
                );
            }

            var valuesToFill = new Content(tc);
            return valuesToFill;
        }
    }
}
