﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Models
{
    class ConfigModels
    {
        public string PathToTemplatesDirectory { get; set; }
        public string PathToFilesDirectory { get; set; }
        public string PathToCurentTemplate { get; set; }
    }
}
