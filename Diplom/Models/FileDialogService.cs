﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Diplom.Models
{
    class FileDialogService
    {
        public FileDialogService()
        {
           
        }

        public string OpenDirectory(string path = "")
        {
            if (Directory.Exists(path))
            {

                using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                {
                    var directoryInfo = Directory.CreateDirectory(path);
                    dialog.SelectedPath = directoryInfo.FullName;
                    System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        return dialog.SelectedPath;
                    }
                }
            }
            else
            {
                using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                {
                    System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        return dialog.SelectedPath;
                    }
                }
            }
            return null;
        }


        public string SelectTemplateFile(string path = "")
        {
            
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "docx file|*.docx";
           
            if (Directory.Exists(path))
            {
                var directoryInfo = Directory.CreateDirectory(path);
                openFileDialog.InitialDirectory = directoryInfo.FullName;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    return openFileDialog.FileName;
                }
            }
            else
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    return openFileDialog.FileName;
                }
            }

            return null;
        }

        public string AddTemplate(string path)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "docx file|*.docx";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.Copy(openFileDialog.FileName, path+"/"+Path.GetFileName(openFileDialog.FileName));
                return path + "/" + Path.GetFileName(openFileDialog.FileName);
            }
            return "";
        }
    }
}
