﻿using Diplom.Entity;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Diplom
{

    class ObjectToBoolConvector2 : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class ObjectToBoolConvector: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Diplom.Entity.Subunit && value!=null)
            {
                return !((Diplom.Entity.Subunit)value).IsRoot;
            }

            //if (value is Diplom.Entity.Requisition)
            //{
            //    return ((Diplom.Entity.Requisition)value).PeopleCount < 1 ? false : true;
            //}

            return value != null ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class SubunitToBoolConvector : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class BooltoTextConvector : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? "з відзнакою" : "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    class SextoTextConvector : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((Sex)value)== Sex.Man ? "чоловіча" : "жіноча";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    class PersonToBoolConvector : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var person = ((Person)value);

            if( person.DateOfBirth == null ||
                string.IsNullOrEmpty(person.DiplomaNumber) ||
                string.IsNullOrEmpty(person.DocomentName) ||
                string.IsNullOrEmpty(person.DiplomaSeries) ||
                string.IsNullOrEmpty(person.DocumentNumber) ||
                string.IsNullOrEmpty(person.FatherName) ||
                string.IsNullOrEmpty(person.FatherNameEn) ||
                string.IsNullOrEmpty(person.Name) ||
                string.IsNullOrEmpty(person.NameEn) ||
                string.IsNullOrEmpty(person.SurnameEn) ||
                string.IsNullOrEmpty(person.Surname) ||
                string.IsNullOrEmpty(person.FatherName))
            {
                return false;
            }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
