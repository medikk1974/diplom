﻿using Diplom.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Diplom.Entity
{
    public class Subunit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsRoot { get; set; }
        public virtual ObservableCollection<Requisition> Requisitions { get; set; }
        public virtual ObservableCollection<HistoricalName> HistoricalNames { get; set; }
        public int? ParentId { get; set; }
        public virtual Subunit Parent { get; set; }
        public virtual ObservableCollection<Subunit> Children { get; set; }
        public virtual ObservableCollection<Faculty> Faculties { get; set; }
        public virtual ObservableCollection<DocumentType> DocumentType { get; set; }

        [NotMapped]
        public ObservableCollection<HistoricalName> HistoricalNamesAll { get { return GetAllHistoricalNamesTop(); } }
        [NotMapped]
        public ObservableCollection<Requisition> RequisitionsAll { get { return GetALLRequisitionButton(); } }
        [NotMapped]
        public ObservableCollection<Faculty> FacultiesAll { get { return GetAllFacultys(); } }
        [NotMapped]
        public ObservableCollection<DocumentType> DocumentTypeAll { get { return GetAllDocumentType(); } }


        public Subunit()
        {
            Faculties = new ObservableCollection<Faculty>();
            Children = new ObservableCollection<Subunit>();
            HistoricalNames = new ObservableCollection<HistoricalName>();
            DocumentType = new ObservableCollection<DocumentType>();
            Requisitions = new ObservableCollection<Requisition>();
        }

        private ObservableCollection<Requisition> GetALLRequisitionButton()
        {
            Queue<Subunit> Q = new Queue<Subunit>();
            HashSet<Subunit> S = new HashSet<Subunit>();

            ObservableCollection<Requisition> allRequisition = new ObservableCollection<Requisition>();

            Q.Enqueue(this);
            S.Add(this);
            while (Q.Count > 0)
            {
                Subunit e = Q.Dequeue();
                allRequisition.AddRange(e.Requisitions.Where(te => te.IsDelited == false).ToList());
                foreach (Subunit emp in e.Children)
                {
                    if (!S.Contains(emp))
                    {
                        Q.Enqueue(emp);
                        S.Add(emp);
                    }
                }
            }
            return allRequisition;
        }
        private ObservableCollection<HistoricalName> GetAllHistoricalNamesTop()
        {
            Queue<Subunit> Q = new Queue<Subunit>();
            HashSet<Subunit> S = new HashSet<Subunit>();

            ObservableCollection<HistoricalName> allHistoricalName = new ObservableCollection<HistoricalName>();

            Q.Enqueue(this);
            S.Add(this);
            while (Q.Count > 0)
            {
                Subunit e = Q.Dequeue();
                if (e.HistoricalNames != null)
                {
                    allHistoricalName.AddRange(e.HistoricalNames.Where(to => to.IsDelited == false).ToList());
                }

                if (e.Parent != null)
                {
                    if (!S.Contains(e.Parent))
                    {
                        Q.Enqueue(e.Parent);
                        S.Add(e.Parent);
                    }
                }
            }
            return allHistoricalName;
        }
        private ObservableCollection<Faculty> GetAllFacultys()
        {
            ObservableCollection<Faculty> faculties = new ObservableCollection<Faculty>();
            
            Queue<Subunit> Q = new Queue<Subunit>();
            HashSet<Subunit> S = new HashSet<Subunit>();
            Q.Enqueue(this);
            
            while (Q.Count > 0)
            {
                Subunit subunit = Q.Dequeue();
                if (!subunit.Parent.IsRoot)
                {
                    Q.Enqueue(subunit.Parent);
                }
                else
                {
                    Q.Enqueue(subunit);
                    break;
                }
            }

            while (Q.Count > 0)
            {
                Subunit e = Q.Dequeue();
                S.Add(e);
                faculties.AddRange(e.Faculties.Where(fa=>fa.IsDelited == false).ToList());
                foreach (Subunit emp in e.Children)
                {
                    if (!S.Contains(emp))
                    {
                        Q.Enqueue(emp);
                        S.Add(emp);
                    }
                }
            }

            return faculties;
        }
        private ObservableCollection<DocumentType> GetAllDocumentType()
        {
            ObservableCollection<DocumentType> document = new ObservableCollection<DocumentType>();

            Queue<Subunit> Q = new Queue<Subunit>();
            HashSet<Subunit> S = new HashSet<Subunit>();

            Q.Enqueue(this);

            while (Q.Count > 0)
            {
                Subunit subunit = Q.Dequeue();
                if (!subunit.Parent.IsRoot)
                {
                    Q.Enqueue(subunit.Parent);
                }
                else
                {
                    Q.Enqueue(subunit);
                    break;
                }
            }

            while (Q.Count > 0)
            {
                Subunit e = Q.Dequeue();
                S.Add(e);
                document.AddRange(e.DocumentType.Where(fa => fa.IsDelited == false).ToList());
                foreach (Subunit emp in e.Children)
                {
                    if (!S.Contains(emp))
                    {
                        Q.Enqueue(emp);
                        S.Add(emp);
                    }
                }
            }
            return document;
        }
    }
}
