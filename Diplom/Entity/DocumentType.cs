﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class DocumentType
    {
        public int Id { get; set; }
        public string Cvalification { get; set; }
        public string CvalificationEn { get; set; }
        public string ProfCvalification { get; set; }
        public string ProfCvalificationEn { get; set; }
        public int? SubunitId { get; set; }
        public Subunit Subunit { get; set; }
        public bool IsDelited { get; set; }
    }
}
