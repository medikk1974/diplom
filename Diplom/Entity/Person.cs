﻿using Diplom.Extensions;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class Person 
    {
        public int Id { get; set; }
        public Sex Sex { get; set; }
        public DateTime DateOfBirth { get; set; } = DateTime.Now;
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public int RequisitionId { get; set; }
        public Requisition Requisition { get; set; }
        public string DiplomaSeries { get; set; }
        public string DiplomaNumber { get; set; }
        public string DocomentName { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentSeries { get; set; }

        public string NameEn { 
            get { return (String.IsNullOrEmpty(_nameEn)) ? Name.TranslitEN() : _nameEn; }
            set { _nameEn = value; }
        }

        public string SurnameEn
        {
            get { return (String.IsNullOrEmpty(_surnameEn)) ? Surname.TranslitEN() : _surnameEn; }
            set { _surnameEn = value; }
        }

        public string FatherNameEn
        {
            get { return (String.IsNullOrEmpty(_fatherNameEn)) ? FatherName.TranslitEN() : _fatherNameEn; }
            set { _fatherNameEn = value; }
        }

  
  
        private string _nameEn;
        private string _surnameEn;
        private string _fatherNameEn;

        public Person()
        {

        }
    }
}
