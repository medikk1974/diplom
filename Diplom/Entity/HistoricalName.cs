﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class HistoricalName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameEN { get; set; }
        public bool IsDelited { get; set; }
        public DateTime DateChange { get; set; }
        public int SubunitId { get; set; }
    }
}
