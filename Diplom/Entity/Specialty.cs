﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class Specialty
    {
        public int Id { get; set; }
        public string Сode { get; set; }

        public string TrainingDirection { get; set; }
        public string TrainingDirectionEn { get; set; }
    
        public string Specialization { get; set; }
        public string SpecializationEn { get; set; }

        public string EducationalProgram { get; set; }
        public string EducationalProgramEn { get; set; }

        public string ProfessionalQualification { get; set; }
        public string ProfessionalQualificationEn { get; set; }
        public int? FacultyId { get; set; }
        public virtual Faculty Faculty { get; set; }
        public bool IsDelited { get; set; }
    }
}
