﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class FormEducation
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
