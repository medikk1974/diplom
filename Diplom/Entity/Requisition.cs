﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Diplom.Entity
{
    public class Requisition
    {
        public int Id { get; set; }
        public DateTime DateTimeCreate { get; set; } = DateTime.Now;
        public string FormEducation { get; set; }
        public DateTime DateEndEducation { get; set; }
        public int DocumentTypeId { get; set; }
        public DocumentType DocumentType { get; set; }

        public int SpecialtyId { get; set; }
        public Specialty Specialty { get; set; }

        public int FacultyId { get; set; }
        public Faculty Faculty { get; set; }

        public int SubunitId { get; set; }
        public Subunit Subunit { get; set; }
        public ObservableCollection<Person> People { get; set; }
        public bool Honor { get; set; }
        public bool IsDelited { get; set; }

        [NotMapped]
        public HistoricalName HistoricalName { 
            get { return GetHistoricalName(); }
        }
        
        [NotMapped]
        public int PeopleCount
        {
            get => People.Count;
        }

        public Requisition()
        {
            People = new ObservableCollection<Person>();
        }

        public Requisition(string formEducation, DateTime dataEnd, DocumentType documentType,
            Faculty faculty, Specialty specialty, bool honor)
        {
            this.FacultyId = faculty.Id;
            this.Faculty = faculty;
            this.FormEducation = formEducation;
            this.DateEndEducation = dataEnd;
            this.DocumentTypeId = documentType.Id;
            this.DocumentType = documentType;
            this.Specialty = specialty;
            this.SpecialtyId = specialty.Id;

            People = new ObservableCollection<Person>();
            this.Honor = honor;
        }

        public HistoricalName GetHistoricalName()
        {
            var temp = Subunit.HistoricalNamesAll.OrderByDescending(e=> e.DateChange)
               .FirstOrDefault(t => t.DateChange <= this.DateEndEducation);
            return temp;
        }
    }

}
