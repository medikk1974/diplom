﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class PassportData
    {
        public int Id { get; set; }
        public string NameDocument {get; 
            set;}
        public string Series { get; set; }
        public string Number { get; set; }
    }
}
