﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Entity
{
    public class Faculty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ObservableCollection<Specialty> Specialties { get ; set; }

        [NotMapped]
        public ObservableCollection<Specialty> SpecialtiesNoDell { get { return new ObservableCollection<Specialty>(Specialties.Where(e => e.IsDelited == false).ToList()); } set { } }
        public int? SubunitId { get; set; }
        public bool IsDelited { get; set; }
        public Faculty()
        {
            Specialties = new ObservableCollection<Specialty>();
        }
    }
}
