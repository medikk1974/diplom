﻿using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.DbContexts
{
    class DiplomDbContext : DbContext
    {
        public DbSet<Subunit> Subunit { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<HistoricalName> HistoricalNames { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Requisition> Requisitions { get; set; }
        public DbSet<Specialty> Specialties { get; set; }

        public DiplomDbContext() : base("DBConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subunit>()
            .HasKey(x => x.Id);

            modelBuilder.Entity<Subunit>().
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Subunit>().
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(255);

            modelBuilder.Entity<Subunit>().
            HasOptional(x => x.Parent)
                .WithMany(x => x.Children)
                .HasForeignKey(x => x.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
