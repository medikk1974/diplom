﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Extensions
{
	public static class StringExtension
	{
		private static Dictionary<string, string> en_to_ru = new Dictionary<string, string>()
		{
			["а"] = "a",
			["б"] = "b",
			["в"] = "v",
			["г"] = "h",
			["ґ"] = "g",
			["д"] = "d",
			["е"] = "e",
			["є"] = "ie",
			["ё"] = "jo",
			["ж"] = "zh",
			["з"] = "z",
			["и"] = "y",
			["і"] = "i",
			["ї"] = "i",
			["й"] = "i",
			["к"] = "k",
			["л"] = "l",
			["м"] = "m",
			["н"] = "n",
			["о"] = "o",
			["п"] = "p",
			["р"] = "r",
			["с"] = "s",
			["т"] = "t",
			["у"] = "u",
			["ф"] = "f",
			["х"] = "kh",
			["ц"] = "ts",
			["ч"] = "ch",
			["ш"] = "sh",
			["щ"] = "shch",
			["ъ"] = "",
			["ы"] = "y",
			["ь"] = "",
			["э"] = "e",
			["ю"] = "iu",
			["я"] = "ia",

			["і"] = "i",
			["ї"] = "i",

			["А"] = "A",
			["Б"] = "B",
			["В"] = "V",
			["Г"] = "H",
			["Ґ"] = "G",
			["Д"] = "D",
			["Е"] = "E",
			["Ё"] = "Jo",
			["Ж"] = "Zh",
			["З"] = "Z",
			["И"] = "Y",
			["Й"] = "Y",
			["К"] = "K",
			["Л"] = "L",
			["М"] = "M",
			["Н"] = "N",
			["О"] = "O",
			["П"] = "P",
			["Р"] = "R",
			["С"] = "S",
			["Т"] = "T",
			["У"] = "U",
			["Ф"] = "F",
			["Х"] = "Kh",
			["Ц"] = "Ts",
			["Ч"] = "Ch",
			["Ш"] = "Sh",
			["Щ"] = "Shch",
			["Ъ"] = "",
			["Ы"] = "Y",
			["Ь"] = "",
			["Э"] = "E",
			["Ю"] = "Yu",
			["Я"] = "Ya",
			["’"] = "",

			["І"] = "I",
			["Ї"] = "Yi",
			["Є"] = "Ye"
		};
		public static string TranslitEN(this string str)
		{
			if(str == null)
            {
				return "";
            }
			var t = str.Trim();
			var nameVal = t.ToCharArray().ToList().Select(e=> Char.ToString(e)).ToList();

			var trans = "";

			for (int i = 0; i < nameVal.Count; i++)
			{
                foreach (var item in en_to_ru)
                {
					var val = en_to_ru[item.Key];
					if(item.Key == nameVal[i])
                    {
						trans += val;
						break;
                    }
                    else if(item.Key == "Є")
                    {
						trans += nameVal[i];
					}
				}
			};

			return trans;
		}
	}
}
