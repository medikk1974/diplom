﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Extensions
{
    public static class ObservableCollectioneExtension
    {
        public static void AddRange<T>(this ObservableCollection<T> col, ICollection<T> array)
        {
            foreach (var item in array)
            {
                col.Add(item);
            }
        }
    }
}
