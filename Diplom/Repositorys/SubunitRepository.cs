﻿using Diplom.DbContexts;
using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Repositorys
{
    class SubunitRepository
    {
        private DiplomDbContext dbContext;
        public SubunitRepository(DiplomDbContext diplomDbContext)
        {
            dbContext = diplomDbContext;
        }
        
        public Subunit GetSubunit()
        {
            return dbContext.Subunit.FirstOrDefault(e => e.IsRoot);
        }

        public Subunit GetSubunit(int Id)
        {
            return dbContext.Subunit
                .FirstOrDefault(e => e.Id == Id);
        }

        public void RemuveSubunit(Subunit subunit)
        {
            dbContext.Subunit.Remove(subunit);
            SaveChanges();
        }

        internal void AddSubunit(Subunit subunit)
        {
            dbContext.Subunit.Add(subunit);
            SaveChanges();
        }

        internal void UpdateSubunit(Subunit subunit)
        {
            dbContext.Entry(subunit).State = EntityState.Modified;
            SaveChanges();
        }

        private void SaveChanges()
        {
            dbContext.SaveChanges();
        }
    }
}
