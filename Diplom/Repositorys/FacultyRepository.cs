﻿using ControlzEx.Standard;
using Diplom.DbContexts;
using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Diplom.Repositorys
{
    class FacultyRepository
    {
        DiplomDbContext dbContext;
        public FacultyRepository(DiplomDbContext diplomDbContext)
        {
            dbContext = diplomDbContext;
        }
        public void AddFacultety(Faculty faculty)
        {
            dbContext.Faculties.Add(faculty);
            SaveChanges();
        }
        public void UpdateFaculty(Faculty facility)
        {
            dbContext.Entry(facility).State = EntityState.Modified;
            SaveChanges();
        }
        private void SaveChanges()
        {
            dbContext.SaveChanges();
        }
    }
}
