﻿using Diplom.DbContexts;
using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Repositorys
{
    class PeopleRepository
    {
        private DiplomDbContext dbContext;

        public PeopleRepository(DiplomDbContext diplomDbContext)
        {
            dbContext = diplomDbContext;
        }
        
        public void AddPerson(Person person)
        {
            dbContext.People.Add(person);
            SaveChange();
        }
        
        public void SaveChange()
        {
            dbContext.SaveChanges();
        }

        public List<Person> GetPeopleByRequisitionId(int requisitionId)
        {
            var t = dbContext.People.Where(person => person.RequisitionId == requisitionId).ToList();
            return t;
        }

        public void DelitePerson(Person person)
        {
            dbContext.People.Remove(person);
            SaveChange();
        }
    }
}
