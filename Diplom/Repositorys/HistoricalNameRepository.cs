﻿using Diplom.DbContexts;
using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Repositorys
{
    class HistoricalNameRepository
    {
        private DiplomDbContext dbContext;
        public HistoricalNameRepository(DiplomDbContext diplomDbContext)
        {
            dbContext = diplomDbContext;
        }

        public void Update(HistoricalName historicalName)
        {
            dbContext.HistoricalNames.AddOrUpdate(historicalName);
            SaveChange();
        }

        public void AddHistoricalName(HistoricalName historicalName, Subunit subunit )
        {
            var getSubunit = dbContext.Subunit.FirstOrDefault(e => e.Id == subunit.Id);
            if(getSubunit != null)
            {
                dbContext.HistoricalNames.Add(historicalName);
            }
            dbContext.SaveChanges();
        }

        public void SaveChange()
        {
            dbContext.SaveChanges();
        }

        internal void Delite(HistoricalName selectedHistoricalName)
        {
            dbContext.HistoricalNames.AddOrUpdate(selectedHistoricalName);
            SaveChange();
        }
    }
}
