﻿using Diplom.DbContexts;
using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Repositorys
{
    class RequisitionRepository
    {
        private DiplomDbContext dbContext;
        public RequisitionRepository(DiplomDbContext diplomDbContext)
        {
            dbContext = diplomDbContext;
        }

        public void AddRequisition(Requisition requisition)
        {
            dbContext.Requisitions.Add(requisition);
            SaveChanges();
        }

        public void UpdateRequestion(Requisition requisition)
        {
            dbContext.Entry(requisition).State = EntityState.Modified;
            SaveChanges();
        }

        public void SaveChanges()
        {
            dbContext.SaveChanges();
        }

        internal Requisition GetRequisition(int id)
        {
            return dbContext.Requisitions.FirstOrDefault(e => e.Id == id); 
        }
    }
}
