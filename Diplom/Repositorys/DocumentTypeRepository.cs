﻿using Diplom.DbContexts;
using Diplom.Entity;
using System.Data.Entity;

namespace Diplom.Repositorys
{
    class DocumentTypeRepository
    {
        DiplomDbContext dbContext;
        public DocumentTypeRepository(DiplomDbContext diplomDbContext)
        {
            dbContext = diplomDbContext;
        }

        public void DeliteDocumentType(DocumentType documentType)
        {
            documentType.IsDelited = true;
            dbContext.Entry(documentType).State = EntityState.Modified;
            SaveChanges();
        }

        public void ChnageDocumentType(DocumentType documentType)
        {
            dbContext.Entry(documentType).State = EntityState.Modified;
            SaveChanges();
        }
        public void AddDocumentType(DocumentType documentType)
        {
            dbContext.DocumentTypes.Add(documentType);
            SaveChanges();
        }

        public void SaveChanges()
        {
            dbContext.SaveChanges();
        }
    }
}
