﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Dto
{
    class SubunitDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsRoot { get; set; }
        public int? ParentId { get; set; }
        public ObservableCollection<SubunitDto> Children { get; set; }
    }
}
