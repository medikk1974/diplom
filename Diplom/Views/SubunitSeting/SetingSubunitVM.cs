﻿using Diplom.Entity;
using Diplom.Repositorys;
using Diplom.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.Entity.Core.Metadata.Edm;

namespace Diplom.Views.SubunitSeting
{
    class SetingSubunitVM : INotifyPropertyChanged
    {
        private string _name;
        private string _nameEn;
        private DateTime _dateTime;
        
        private Subunit selectedTreeViewItem;

        private HistoricalNameRepository historicalNameRepository;

        public string Name { get { return _name; } set { _name = value; } }
        public string NameEn { get { return _nameEn; } set { _nameEn = value; } }
        public string FacultyName { get; set; }
        public DateTime DateChange { get { return _dateTime; } set { _dateTime = value; } }
        public HistoricalName _selectedHistoricalName { get; set; }
        public HistoricalName SelectedHistoricalName 
        { 
            get { return _selectedHistoricalName; } 
            set { _selectedHistoricalName = value; OnPropertyChanged(nameof(SelectedHistoricalName));}
        }
        public ObservableCollection<Faculty> Faculties { get; set; }
        public ObservableCollection<HistoricalName> HistoricalNames { get; set; }
        public ObservableCollection<DocumentType> DocumentTypes { get; set; }
        public FacultyRepository FacultyRepository { get; set; }
        public SubunitRepository SubunitRepository { get; set; }
        public DocumentTypeRepository documentTypeRepository { get; set; }
        

        #region Commands
        public RelayCommands AddCom { get; set; }
        public RelayCommands DelCom { get; set; }
        public RelayCommands ChangeCom { get; set; }

        public RelayCommands AddFacCM { get; set; }
        public RelayCommands DelitFacCM { get; set; }
        public RelayCommands ChangeFaCM { get; set; }
       
        public RelayCommands AddSpecialtyCM { get; set; }
        public RelayCommands DelitSpecialtyCM { get; set; }
        public RelayCommands ChangeSpecialtyCM { get; set; }


        public RelayCommands AddDocumentTypeCM { get; set; }
        public RelayCommands DelitDocumentTypeCM { get; set; }
        public RelayCommands ChangeDocumentTypeCM { get; set; }

        #endregion

        private Faculty _selectedFaculty;
        public Faculty SlectedFaculty  {
            get { return _selectedFaculty; } 
            set { _selectedFaculty = value;
                OnPropertyChanged(nameof(SlectedFaculty));
                OnPropertyChanged("SpecialtiesAll"); 
            } 
        }
        public ObservableCollection<Specialty> SpecialtiesAll { get { return SlectedFaculty == null? new ObservableCollection<Specialty>() : new ObservableCollection<Specialty>(SlectedFaculty.Specialties.Where(e=> e.IsDelited == false)) ; } 
            set { SlectedFaculty.Specialties = value; } }
        public Specialty Specialty { get; set; }
        public Specialty _selectedSpecialti { get; set; }
        public Specialty SelectedSpecialti { get { return _selectedSpecialti; } set { _selectedSpecialti = value; OnPropertyChanged(nameof(SelectedSpecialti));} }

        public DocumentType DocumentType { get; set; }

        public DocumentType _selectedDocumentType { get; set; }
        public DocumentType SelectedDocumentType { 
            get { return _selectedDocumentType; } 
            set { _selectedDocumentType = value; OnPropertyChanged(nameof(SelectedDocumentType)); } 
        }

        public SetingSubunitVM(Subunit selectedTreeViewItem,
            HistoricalNameRepository historicalNameRepository,
            FacultyRepository facultyRepository,
            SubunitRepository subunitRepository,
            DocumentTypeRepository documentTypeRepository
            )
        {
            this.selectedTreeViewItem = selectedTreeViewItem;

            this.HistoricalNames =  new ObservableCollection<HistoricalName>(selectedTreeViewItem.HistoricalNamesAll);
            this.Faculties = new ObservableCollection<Faculty>(selectedTreeViewItem.FacultiesAll);
            this.DocumentTypes = new ObservableCollection<DocumentType>(selectedTreeViewItem.DocumentTypeAll);
            this.documentTypeRepository = documentTypeRepository;

            this.historicalNameRepository = historicalNameRepository;
            this.FacultyRepository = facultyRepository;
            this.SubunitRepository = subunitRepository;

            this.Specialty = new Specialty();
            DocumentType = new DocumentType();


            _dateTime = DateTime.Now;
            AddCom = new RelayCommands(AddHistoricalNmae);
            DelCom = new RelayCommands(DeliteHistoricalNmae);
            ChangeCom = new RelayCommands(ChangeHistoricalNmae);

            AddFacCM = new RelayCommands(AddFacultet);
            DelitFacCM = new RelayCommands(DeliteFacultety);
            ChangeFaCM = new RelayCommands(ChangeFacultety);

            AddSpecialtyCM = new RelayCommands(AddSpecialty);
            DelitSpecialtyCM = new RelayCommands(DelitSpecialty);
            ChangeSpecialtyCM = new RelayCommands(ChangeSpecialty);

            AddDocumentTypeCM = new RelayCommands(AddDocumentType);
            DelitDocumentTypeCM = new RelayCommands(DelitDocumentType);
            ChangeDocumentTypeCM = new RelayCommands(ChangeDocumentType);
        }

        public SetingSubunitVM()
        {

        }


      
        private void ChangeDocumentType()
        {
            if (CheckAllFillDocumentType())
            {
                SelectedDocumentType.ProfCvalification = DocumentType.ProfCvalification;
                SelectedDocumentType.ProfCvalificationEn = DocumentType.ProfCvalificationEn;
                SelectedDocumentType.Cvalification = DocumentType.Cvalification;
                SelectedDocumentType.CvalificationEn = DocumentType.CvalificationEn;

                documentTypeRepository.ChnageDocumentType(SelectedDocumentType);
            }
            UpdateDocumentType();

        }

        private void DelitDocumentType()
        {
            if (SelectedDocumentType != null)
            {
                documentTypeRepository.DeliteDocumentType(SelectedDocumentType);
            }
            UpdateDocumentType();
        }

        private void AddDocumentType()
        {
            if (CheckAllFillDocumentType())
            {
                DocumentType.SubunitId = selectedTreeViewItem.Id;
                documentTypeRepository.AddDocumentType(DocumentType);
            }
            UpdateDocumentType();
        }
        private bool CheckAllFillDocumentType()
        {
            if (!String.IsNullOrEmpty(DocumentType.ProfCvalification) &&
                !String.IsNullOrEmpty(DocumentType.ProfCvalificationEn) &&
                !String.IsNullOrEmpty(DocumentType.Cvalification) &&
                !String.IsNullOrEmpty(DocumentType.CvalificationEn)
                )
            {
                return true;
            }
            return false;
        }
        private void ChangeSpecialty()
        {
            if (SelectedSpecialti != null && SlectedFaculty != null)
            {
                SelectedSpecialti.Specialization = Specialty.Specialization;
                SelectedSpecialti.SpecializationEn = Specialty.SpecializationEn;
                SelectedSpecialti.ProfessionalQualification = Specialty.ProfessionalQualification;
                SelectedSpecialti.ProfessionalQualificationEn = Specialty.ProfessionalQualificationEn;
                SelectedSpecialti.EducationalProgram = Specialty.EducationalProgram;
                SelectedSpecialti.EducationalProgramEn = Specialty.EducationalProgramEn;
                SelectedSpecialti.TrainingDirection = Specialty.TrainingDirection;
                SelectedSpecialti.TrainingDirectionEn = Specialty.TrainingDirectionEn;
                SelectedSpecialti.Сode = Specialty.Сode;
                
                SubunitRepository.UpdateSubunit(selectedTreeViewItem);
                OnPropertyChanged("SpecialtiesAll");
            }
        }
        private void DelitSpecialty()
        {
            if(SelectedSpecialti != null && SlectedFaculty != null)
            {
                SelectedSpecialti.IsDelited = true;
                SubunitRepository.UpdateSubunit(selectedTreeViewItem);
                OnPropertyChanged("SpecialtiesAll");
            }
        }
        private void AddSpecialty()
        {
            if (CheckAllFillInNewSpecialty() && SlectedFaculty!=null)
            {
                SlectedFaculty.Specialties.Add(Specialty);
                Specialty = new Specialty();
                SubunitRepository.UpdateSubunit(selectedTreeViewItem);
                OnPropertyChanged("Specialty");
                OnPropertyChanged("SpecialtiesAll");
            }
            else
            {
                MessageBox.Show("Не всі позиції спеціальності заповнені");
            }
        }
        
        private void ChangeFacultety()
        {
            if (!string.IsNullOrEmpty(FacultyName))
            {
                SlectedFaculty.Name = FacultyName;
                FacultyRepository.UpdateFaculty(SlectedFaculty);
            }
            UpdateFacultets();
        }
        private void DeliteFacultety()
        {
            SlectedFaculty.IsDelited = true;
            FacultyRepository.UpdateFaculty(SlectedFaculty);
            UpdateFacultetsWhereDelit();
        }
        private void AddFacultet()
        {
            if (!string.IsNullOrEmpty(FacultyName))
            {
                var facult = new Faculty()
                {
                    Name = FacultyName,
                    SubunitId = this.selectedTreeViewItem.Id
                };
                selectedTreeViewItem.Faculties.Add(facult);
                SubunitRepository.UpdateSubunit(selectedTreeViewItem);
                Faculties.Add(facult);
                UpdateFacultets();
            }
        }


        private void ChangeHistoricalNmae()
        {
            if (!string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(NameEn) && _dateTime != null)
            {
                SelectedHistoricalName.Name = Name;
                SelectedHistoricalName.NameEN = NameEn;
                SelectedHistoricalName.DateChange = _dateTime;
                historicalNameRepository.Update(SelectedHistoricalName);
            }
            else
            {
                MessageBox.Show("Не всі поля заповнені");
            }
            UpdateHistoryNameWindow();
        }
        public void DeliteHistoricalNmae()
        {
            if (SelectedHistoricalName != null)
            {
                SelectedHistoricalName.IsDelited = true;
                historicalNameRepository.Delite(SelectedHistoricalName);
            }
            else
            {
                MessageBox.Show("Не вибрано елемент");
            }
            UpdateHistoryNameWindow();
        }
        private void AddHistoricalNmae()
        {
            if(!string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(NameEn) && _dateTime!=null )
            {
                var his = new HistoricalName()
                {
                    SubunitId = selectedTreeViewItem.Id,
                    DateChange = this.DateChange,
                    Name = this.Name,
                    NameEN = this.NameEn
                };
                historicalNameRepository.AddHistoricalName(his, selectedTreeViewItem);
                UpdateHistoryNameWindow();
            }
            else
            {
                MessageBox.Show("Не всі поля заповнені");
            }
        }

        private void UpdateHistoryNameWindow()
        {
            HistoricalNames.Clear();
            HistoricalNames.AddRange(selectedTreeViewItem.HistoricalNamesAll.ToList());

            NameEn = "";
            Name = "";
            OnPropertyChanged(nameof(Name));
            OnPropertyChanged(nameof(NameEn));
        }
        private void UpdateFacultets()
        {
            FacultyName = "";
            OnPropertyChanged(nameof(FacultyName));
            var old = Faculties.ToList();
            Faculties.Clear();
            Faculties.AddRange(old);
        }
        private void UpdateFacultetsWhereDelit()
        {
            FacultyName = "";
            OnPropertyChanged(nameof(FacultyName));
            Faculties.Remove(SlectedFaculty);
        }

        private void UpdateDocumentType()
        {
            DocumentType = new DocumentType();
            OnPropertyChanged("DocumentType");
            DocumentTypes.Clear();
            DocumentTypes.AddRange(selectedTreeViewItem.DocumentTypeAll.ToList());
            OnPropertyChanged("DocumentTypes");
        }
       
        private bool CheckAllFillInNewSpecialty()
        {
            if (string.IsNullOrEmpty(Specialty.EducationalProgram) &&
                string.IsNullOrEmpty(Specialty.EducationalProgramEn) &&
                string.IsNullOrEmpty(Specialty.TrainingDirection) &&
                string.IsNullOrEmpty(Specialty.TrainingDirectionEn) &&
                string.IsNullOrEmpty(Specialty.Specialization) &&
                string.IsNullOrEmpty(Specialty.SpecializationEn) &&
                string.IsNullOrEmpty(Specialty.EducationalProgram) &&
                string.IsNullOrEmpty(Specialty.EducationalProgramEn) &&
                string.IsNullOrEmpty(Specialty.ProfessionalQualification) &&
                string.IsNullOrEmpty(Specialty.ProfessionalQualificationEn)
                )
            {
                return false;
            }
            return true;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
