﻿using Diplom.Entity;
using Diplom.Repositorys;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace Diplom.Views.StudentApplicationForm
{
    class StudentApplicationFormVM : INotifyPropertyChanged
    {
        public PeopleRepository PeopleRepository { get; set; }
        public Entity.Requisition SelectedRequisition { get; set; }
        public RelayCommands AcceptCom { get; set; }
        public Sex Sex
        {
            get{ return Person.Sex;}
            set{ Person.Sex = value; OnPropertyChanged(nameof(Sex)); }
        }
        public string Name{
            get { return Person.Name; }
            set { Person.Name = value;
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(NameEn));
                OnPropertyChanged(nameof(Person));
            }
        }
        public string Surname
        {
            get  { return Person.Surname; }
            set { Person.Surname = value; 
                OnPropertyChanged(nameof(Surname));
                OnPropertyChanged(nameof(SurnameEn));
                OnPropertyChanged(nameof(Person));
            }
        }
        public string FatherName
        {
            get { return Person.FatherName; }
            set { Person.FatherName = value; 
                OnPropertyChanged(nameof(FatherName));
                OnPropertyChanged(nameof(FatherNameEn));
                OnPropertyChanged(nameof(Person));
            }
        }

        public string NameEn
        {
            get { return Person.NameEn; }
            set { Person.NameEn = value; OnPropertyChanged(nameof(NameEn)); }
        }

        public string SurnameEn
        {
            get { return Person.SurnameEn; }
            set { Person.SurnameEn = value; OnPropertyChanged(nameof(SurnameEn)); }
        }

        public string FatherNameEn
        {
            get { return Person.FatherNameEn; }
            set { Person.FatherNameEn = value; OnPropertyChanged(nameof(Person)); OnPropertyChanged(nameof(FatherNameEn)); }
        }

        public string DiplomaSeries { get => Person.DiplomaSeries;
            set { Person.DiplomaSeries = value; OnPropertyChanged(nameof(Person)); } }
        public string DiplomaNumber { get => Person.DiplomaNumber;
            set { Person.DiplomaNumber = value; OnPropertyChanged(nameof(Person)); } }
        public string DocomentName { get => Person.DocomentName;
            set { Person.DocomentName = value; OnPropertyChanged(nameof(Person)); } }
        public string DocumentNumber { get => Person.DocumentNumber;
            set { Person.DocumentNumber = value; OnPropertyChanged(nameof(Person)); } }
        public string DocumentSeries { get => Person.DocumentSeries;
            set { Person.DocumentSeries = value; OnPropertyChanged(nameof(Person)); } }


        public Person Person { get; set; }
        public StudentApplicationFormVM(Entity.Requisition selectedRequisition, PeopleRepository peopleRepository)
        {
            SelectedRequisition = selectedRequisition;
            Person = new Person();
            Person.DocomentName = "2016 Паспорт громадянина України";

            AcceptCom = new RelayCommands(AddStudent);
            PeopleRepository = peopleRepository;
        
        }

        private void AddStudent()
        {
            Person.RequisitionId = SelectedRequisition.Id;
            Person.Requisition = SelectedRequisition;
            PeopleRepository.AddPerson(Person);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
