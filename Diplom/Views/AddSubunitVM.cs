﻿using Diplom.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplom.Entity;

namespace Diplom.Views
{   
    public class AddSubunitVM
    {
        public Subunit Subunit { get; set; }
        public AddSubunitVM()
        {
            Subunit = new Subunit();
        }
    }
}
