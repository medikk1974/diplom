﻿using Diplom.Entity;
using Diplom.Repositorys;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Views.Requisition
{
    class CreateRequisitionVM
    {
        private Subunit Subunit { get; set; }
        public ObservableCollection<Faculty> Faculties { get; set; }
        public ObservableCollection<DocumentType> DocumentType { get; set; }
        public ObservableCollection<string> FormEducations { get; set; }
        
        public RelayCommands AceptCom { get; set; }
        public RelayCommands CancellCom { get; set; }


        public RequisitionRepository RequisitionRepository { get; set; }

        public string SelectedFormEducation { get; set; }
        public Faculty SelectedFaculty { get; set; }
        public DateTime SelectedDate { get; set; } = DateTime.Now;
        public Specialty SelectedSpecialties { get; set; }
        public DocumentType SelectedDocumentType { get; set; }
        public bool Honors { get; set; } 



        public CreateRequisitionVM(Subunit selectedSubunit, RequisitionRepository requisitionRepository)
        {
            this.Subunit = selectedSubunit;
            Faculties = Subunit.FacultiesAll;
            
            DocumentType = selectedSubunit.DocumentTypeAll;
            this.RequisitionRepository = requisitionRepository;

            FormEducations = new ObservableCollection<string>(new List<string> { "Дена", "Заочна" });

            AceptCom = new RelayCommands(CreateRequisition);
            CancellCom = new RelayCommands(Cancel);
        }

        private void Cancel()
        {

        }

        private void CreateRequisition()
        {
            //todo перевірка вадлідації чи вибрані всі дані.
            var requisition = new Diplom.Entity.Requisition(SelectedFormEducation, SelectedDate,
                SelectedDocumentType, SelectedFaculty, SelectedSpecialties, Honors);
            requisition.SubunitId = Subunit.Id;

            RequisitionRepository.AddRequisition(requisition);
        }

    }
}
