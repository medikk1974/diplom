﻿using Diplom.Entity;
using Diplom.Repositorys;
using Diplom.Views.StudentApplicationForm;
using System.Collections.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplom.Extensions;

namespace Diplom.Views.Requisition
{
    class RequisitionSetingVM
    {
        public RelayCommands AddPersomCom { get; set; }
        public RelayCommands DelitePersonCom { get; set; }
        public Entity.Requisition Requisition { get; set; }
        public RequisitionRepository RequisitionRepository { get; set; }
        public PeopleRepository PeopleRepository { get; set; }
        public Person SelectedPerson { get; set; }

        public string FacultyName { get => this.Requisition.Faculty.Name; }
        public ObservableCollection<Person> People { get; set; }

        public RequisitionSetingVM(Entity.Requisition requisition, RequisitionRepository requisitionRepository, PeopleRepository peopleRepository)
        {
            AddPersomCom = new RelayCommands(AddPerson);
            DelitePersonCom = new RelayCommands(DelitePerson);
            
            Requisition = requisition;
            RequisitionRepository = requisitionRepository;
            PeopleRepository = peopleRepository;
            /*Requisition.People = */ new ObservableCollection<Person>(PeopleRepository.GetPeopleByRequisitionId(Requisition.Id));
            People = requisition.People;
        }

        private void DelitePerson()
        {
            if(SelectedPerson != null)
            {
                PeopleRepository.DelitePerson(SelectedPerson);
            }
        }
        private void AddPerson()
        {
            
            StudentApplicationFormVM vm = new StudentApplicationFormVM(Requisition, PeopleRepository);
            StudentApplicationForm.StudentApplicationForm ui = new StudentApplicationForm.StudentApplicationForm();
            ui.DataContext = vm;

            if (ui.ShowDialog() == true)
            {

            }
            else
            {

            }
        }
    }
}
