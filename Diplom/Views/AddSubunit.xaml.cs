﻿using System.Windows;
using System.Windows.Input;

namespace Diplom.Views
{
    /// <summary>
    /// Логика взаимодействия для AddSubunit.xaml
    /// </summary>
    public partial class AddSubunit : Window
    {
        public AddSubunit(AddSubunitVM addSubunitVM)
        {
            InitializeComponent();
            DataContext = addSubunitVM;
        }
        public AddSubunit()
        {
            InitializeComponent();
        }
        
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
