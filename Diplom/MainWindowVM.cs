﻿using Diplom.DbContexts;
using Diplom.Entity;
using Diplom.Extensions;
using Diplom.Models;
using Diplom.Repositorys;
using Diplom.Views;
using Diplom.Views.Requisition;
using Diplom.Views.SubunitSeting;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity.Core.Metadata.Edm;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.RightsManagement;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Diplom
{
    class MainWindowVM : INotifyPropertyChanged
    {

        public string SlectedTemplateFile { get => File.Exists(Config.ConfigModels.PathToCurentTemplate) ? Config.ConfigModels.PathToCurentTemplate: "Не найдено файл шаблону"; }
        public RelayCommands<Subunit> GetSelectedItenmCM { get; set; }
        public RelayCommands AddTreeNodeCM { get; set; }
        public RelayCommands SetingTreeNodeCM { get; set; }
        public RelayCommands DropTreeNodeCM { get; set; }
        public RelayCommands NewRequisitionCm { get; set; }
        public RelayCommands AddTemplateCm { get; set; }
        public RelayCommands OpenTemplatesFiles { get; set; }
        public RelayCommands DeleteRequisitionCom { get; set; }
        public RelayCommands OpenTemplatesDirectoryCom { get; set; }
        public RelayCommands OpenDocomentsDirectoryCom { get; set; }
        public RelayCommands SelectedTemplatesCom { get; set; }

        private Subunit _selectedSubunit;
        public Subunit SelectedSubunit {
            get { return _selectedSubunit; }
            set { _selectedSubunit = value;
                
                _requestuinSelectSubunit = SelectedSubunit.RequisitionsAll;
                OnPropertyChanged(nameof(SelectedSubunit));
                OnPropertyChanged(nameof(RequestuinSelectSubunit));
            }
        }
        public ObservableCollection<Subunit> Subunits { get; set; }
        private Config Config { get; set; }
        
        private DiplomDbContext dbContext;
        private SubunitRepository SubunitRepository { get; set; }
        private HistoricalNameRepository HistoricalNameRepository { get; set; }
        public PeopleRepository PeopleRepository { get; set; }
        private FacultyRepository FacultyRepository { get; set; }
        public RequisitionRepository RequisitionRepository { get; set; }
        private DocumentTypeRepository DocumentTypeRepository { get; set; }
        public RelayCommands OpenRequisitionsCom { get; set; }

        public RelayCommands SelectDirectoryFoorTemplateCom { get; set; }
        public RelayCommands SelectDirectoryForFilesCom { get; set; }


        private Requisition _selectedRequisition;
        public Requisition SelectedRequisition {
            get { return _selectedRequisition; } 
            set { _selectedRequisition = value; OnPropertyChanged(nameof(SelectedRequisition));}
        }
        public RelayCommands PrintCm { get; set; }
        private FileDialogService _fileDialogService;

        private ObservableCollection<Requisition> _requestuinSelectSubunit;
        public ObservableCollection<Requisition> RequestuinSelectSubunit { 
            get { return _requestuinSelectSubunit; } 
            set { _requestuinSelectSubunit = value; } 
        }

        public MainWindowVM()
        {
            SelectDirectoryFoorTemplateCom = new RelayCommands(SelectDirectoryFoorTemplate);
            SelectDirectoryForFilesCom = new RelayCommands(SelectDirectoryForFiles);
            _fileDialogService = new FileDialogService();
            OpenDocomentsDirectoryCom = new RelayCommands(OpenDocomentsDirectory);
            OpenTemplatesDirectoryCom = new RelayCommands(OpenTemplatesDirectory);
            Config =  new Config();

            dbContext = new DiplomDbContext();
           

            SeedData.SeedSubunit(dbContext);

            SubunitRepository = new SubunitRepository(dbContext);
            DocumentTypeRepository = new DocumentTypeRepository(dbContext);
            RequisitionRepository = new RequisitionRepository(dbContext);
            HistoricalNameRepository = new HistoricalNameRepository(dbContext);
            FacultyRepository = new FacultyRepository(dbContext);
            PeopleRepository = new PeopleRepository(dbContext);


            Subunits = new ObservableCollection<Subunit>(){
                SubunitRepository.GetSubunit()
            };

            NewRequisitionCm = new RelayCommands(NewRequisition);
            
            AddTreeNodeCM = new RelayCommands(AddTreeNode);
            SetingTreeNodeCM = new RelayCommands(SetingTreeNodeSubdevision);
            DropTreeNodeCM = new RelayCommands(DropTreeNode);
            PrintCm = new RelayCommands(PrintselectedReqestion);
            DeleteRequisitionCom = new RelayCommands(DeleteRequisition);

            GetSelectedItenmCM = new RelayCommands<Subunit>(SetSelectedItemTreeView, (e) => true);
            AddTemplateCm = new RelayCommands(AddTempaleFille);

            OpenRequisitionsCom = new RelayCommands(OpenRequisitions);


            SelectedTemplatesCom = new RelayCommands(SelectTemplate);
            ReadAllDatabase();
        }


        private void SelectDirectoryForFiles()
        {
            var pathToDirectory = _fileDialogService.OpenDirectory(Config.ConfigModels.PathToFilesDirectory);
            if(!String.IsNullOrEmpty(pathToDirectory))
            {
                Config.ConfigModels.PathToFilesDirectory = pathToDirectory;
                Config.UpdateConfig();
            }
        }

        private void SelectDirectoryFoorTemplate()
        {
            var pathToDirectory = _fileDialogService.OpenDirectory(Config.ConfigModels.PathToTemplatesDirectory);
            if (!string.IsNullOrEmpty(pathToDirectory))
            {
                Config.ConfigModels.PathToTemplatesDirectory = pathToDirectory;
                Config.UpdateConfig();
            }
        }

        private void OpenTemplatesDirectory()
        {
            _fileDialogService.SelectTemplateFile(Config.ConfigModels.PathToTemplatesDirectory);
        }

        private void OpenDocomentsDirectory()
        {
            _fileDialogService.SelectTemplateFile(Config.ConfigModels.PathToFilesDirectory);
        }

        private void DeleteRequisition()
        {
            SelectedRequisition.IsDelited = true;
            RequisitionRepository.UpdateRequestion(SelectedRequisition);

            var t = SelectedSubunit.RequisitionsAll.ToList();
            RequestuinSelectSubunit.Clear();
            t.ForEach(e => RequestuinSelectSubunit.Add(e));
        }

        public void AddTempaleFille()
        {
            try
            {
                var newTemplate = _fileDialogService.AddTemplate(Config.ConfigModels.PathToTemplatesDirectory);
            }
            catch
            {
                System.Windows.MessageBox.Show($"Файл з даним імям вже існує!!! \n\r Переіменуйте файл!");
            }
        }

        //todo
        private void PrintselectedReqestion()
        {
            if(SelectedRequisition != null)
            {
                if (!string.IsNullOrEmpty(Config.ConfigModels.PathToCurentTemplate))
                {
                    if (File.Exists(Config.ConfigModels.PathToCurentTemplate))
                    {
                        DocumentMaker documentMaker
                        = new DocumentMaker(Config.ConfigModels.PathToCurentTemplate,
                        Config.ConfigModels.PathToFilesDirectory);

                        documentMaker.CreateTemplate(SelectedRequisition);
                    }
                    else
                    {
                       System.Windows.MessageBox.Show($"Файл шаблону не було знайдено");
                    }
                }
            }
        }


        private void SelectTemplate()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "docx file|*.docx";

         
            var directoryInfo = Directory.CreateDirectory(Config.ConfigModels.PathToTemplatesDirectory);
            openFileDialog.InitialDirectory = directoryInfo.FullName;


            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Config.ConfigModels.PathToCurentTemplate = openFileDialog.FileName;
                Config.UpdateConfig();

                OnPropertyChanged(nameof(SlectedTemplateFile));
            }
        }

        private void ReadAllDatabase()
        {
            Task.Run(() =>
            {
                dbContext.HistoricalNames.ToList();
                dbContext.People.ToList();
                dbContext.Requisitions.ToList();
                dbContext.DocumentTypes.ToList();
                dbContext.Faculties.ToList();
                dbContext.Specialties.ToList();
            });
        }

        private void OpenRequisitions()
        {
            if (SelectedRequisition != null)
            {
                RequisitionSetingVM requisitionSetingVM = new RequisitionSetingVM(SelectedRequisition, RequisitionRepository, PeopleRepository);
                RequisitionSeting ui = new RequisitionSeting();
                ui.DataContext = requisitionSetingVM;
                if (ui.ShowDialog() == true)
                {

                }
                else
                {

                }

                var t = RequestuinSelectSubunit.ToList();
                RequestuinSelectSubunit.Clear();
                t.ForEach(e => RequestuinSelectSubunit.Add(e));
            }
        }

        private void NewRequisition()
        {
            if(!SelectedSubunit.IsRoot)
            {
                var createRequisitionVM = new CreateRequisitionVM(SelectedSubunit, RequisitionRepository);
                var createRequisitionUI = new CreateRequisitionUI();

                createRequisitionUI.DataContext = createRequisitionVM;

                if (createRequisitionUI.ShowDialog() == true)
                {
                    OnPropertyChanged("SelectedSubunit");
                }
                else
                {

                }
                var t = SelectedSubunit.RequisitionsAll.ToList();
                RequestuinSelectSubunit.Clear();
                t.ForEach(e => RequestuinSelectSubunit.Add(e));
            }
            else
            {
                //todo check eroro massage
            }
        }

        public void SetSelectedItemTreeView(Subunit Subunit)
        {
            SelectedSubunit = Subunit;
        }
        private void AddTreeNode()
        {
            if(SelectedSubunit != null)
            {
                AddSubunitVM addSubunitVM = new AddSubunitVM();
                AddSubunit addSubunitUI = new AddSubunit(addSubunitVM);

                if (addSubunitUI.ShowDialog() == true)
                {
                    addSubunitVM.Subunit.Parent = SelectedSubunit;

                    SelectedSubunit.Children.Add(addSubunitVM.Subunit);

                    SubunitRepository.AddSubunit(addSubunitVM.Subunit);
                }
            }
        }
        private void SetingTreeNodeSubdevision()
        {
            try
            {
                if (SelectedSubunit != null & SelectedSubunit.IsRoot == false)
                {
                    var setingSubunitVM = new SetingSubunitVM(

                        SelectedSubunit,
                        this.HistoricalNameRepository,
                        FacultyRepository,
                        SubunitRepository,
                        DocumentTypeRepository
                        );
                    SetingSubunit setingSubunit = new SetingSubunit();
                    setingSubunit.DataContext = setingSubunitVM;
                    setingSubunit.Show();
                }
            }
            catch
            {

            }
        }
        private void DropTreeNode()
        {
            try
            {
                if (SelectedSubunit != null & SelectedSubunit.IsRoot == false)
                {
                    SelectedSubunit.Parent.Children.Remove(SelectedSubunit);
                    SubunitRepository.UpdateSubunit(Subunits[0]);
                }
            }
            catch
            { 
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
